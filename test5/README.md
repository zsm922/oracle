###### 班级：20级软件工程4班
###### 学号：202010414429
###### 姓名：周思藐



# 实验5：包，过程，函数的用法

## 实验目的

- 了解PL/SQL语言结构

- 了解PL/SQL变量和常量的声明和使用方法

- 学习包，过程，函数的用法。

  

## 实验内容

1. 创建一个包(Package)，包名是MyPack。

   ![](https://gitlab.com/zsm922/oracle/-/raw/main/test5/pict1.png)

   - 代码

   ```sql
   CREATE OR REPLACE PACKAGE MyPack IS
       /*
       包MyPack中有：
       一个函数:Get_SalaryAmount(V_DEPARTMENT_ID NUMBER)
       一个过程:GET_EMPLOYEES(V_EMPLOYEE_ID NUMBER)
       */
       FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER;
       PROCEDURE GET_EMPLOYEES(V_EMPLOYEE_ID NUMBER);
   END MyPack;
   /
   ```

   

2. 在MyPack中创建一个函数Get_SalaryAmount,输入的参数是部门ID，通过查询员工表，统计每个部门的salay工资总额。

   ![](https://gitlab.com/zsm922/oracle/-/raw/main/test5/pict2.png)

   - 代码

   ```sql
   create or replace PACKAGE BODY MyPack IS
   FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER
   AS
       N NUMBER(20,2);
       BEGIN
       SELECT SUM(salary) into N  FROM EMPLOYEES E
       WHERE E.DEPARTMENT_ID =V_DEPARTMENT_ID;
       RETURN N;
       END;
   ```

   

3. 在MyPack中创建一个过程GET_EMPLOYEES,输入参数是员工ID，在过程中使用游标，通过查询员工表，递归查询某个员工及其所有下属，子下属员工。

   ![](https://gitlab.com/zsm922/oracle/-/raw/main/test5/pict3.png)

  - 代码

  ```sql
  PROCEDURE GET_EMPLOYEES(V_EMPLOYEE_ID NUMBER)
  AS
      LEFTSPACE VARCHAR(2000);
      begin
      --通过LEVEL判断递归的级别
      LEFTSPACE:=' ';
      --使用游标
      for v in
          (SELECT LEVEL,EMPLOYEE_ID, FIRST_NAME,MANAGER_ID FROM employees
          START WITH EMPLOYEE_ID = V_EMPLOYEE_ID
          CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID)
      LOOP
          DBMS_OUTPUT.PUT_LINE(LPAD(LEFTSPACE,(V.LEVEL-1)*4,' ')||
                              V.EMPLOYEE_ID||' '||v.FIRST_NAME);
      END LOOP;
      END;
  END MyPack;
  /
  ```

  

![](https://gitlab.com/zsm922/oracle/-/raw/main/test5/pict6.png)

## 测试

- 函数Get_SalaryAmount()测试

   ![](https://gitlab.com/zsm922/oracle/-/raw/main/test5/pict4.png)

- 过程Get_Employees()测试

   ![](https://gitlab.com/zsm922/oracle/-/raw/main/test5/pict5.png)



## 实验总结

本次实验主要介绍了PL/SQL语言结构、变量和常量的声明与使用方法，以及包、过程、函数的应用。在实验中，我们通过创建名为MyPack的包，并编写Get_SalaryAmount函数和GET_EMPLOYEES过程，掌握了如何使用游标递归查询员工表，统计每个部门的工资总额，以及查询某个员工及其所有下属、子下属员工等技能。这些技能对于PL/SQL程序开发非常有用，可以提高程序效率和可维护性，同时也增加了程序设计的灵活性。此外，本次实验还让我们更深入地了解了Oracle数据库的递归查询语句格式和使用方法，为今后的数据库开发工作奠定了基础。
