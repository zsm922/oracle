###### 班级：20级软件工程4班
###### 学号：202010414429
###### 姓名：周思藐



# 实验4：PL/SQL语言打印杨辉三角

## 实验目的

掌握Oracle PL/SQL语言以及存储过程的编写。

## 实验内容

- 认真阅读并运行下面的杨辉三角源代码。
- 将源代码转为hr用户下的一个存储过程Procedure，名称为YHTriange，存储起来。存储过程接受行数N作为参数。
- 运行这个存储过程即可以打印出N行杨辉三角。
- 写出创建YHTriange的SQL语句。

## 杨辉三角源代码

- 杨辉三角源代码

```sql
set serveroutput on;
declare
type t_number is varray (100) of integer not null; --数组
i integer;
j integer;
spaces varchar2(30) :='   '; --三个空格，用于打印时分隔数字
N integer := 9; -- 一共打印9行数字
rowArray t_number := t_number();
begin
    dbms_output.put_line('1'); --先打印第1行
    dbms_output.put(rpad(1,9,' '));--先打印第2行
    dbms_output.put(rpad(1,9,' '));--打印第一个1
    dbms_output.put_line(''); --打印换行
    --初始化数组数据
    for i in 1 .. N loop
        rowArray.extend;
    end loop;
    rowArray(1):=1;
    rowArray(2):=1;    
    for i in 3 .. N --打印每行，从第3行起
    loop
        rowArray(i):=1;    
        j:=i-1;
            --准备第j行的数组数据，第j行数据有j个数字，第1和第j个数字为1
            --这里从第j-1个数字循环到第2个数字，顺序是从右到左
        while j>1 
        loop
            rowArray(j):=rowArray(j)+rowArray(j-1);
            j:=j-1;
        end loop;
            --打印第i行
        for j in 1 .. i
        loop
            dbms_output.put(rpad(rowArray(j),9,' '));--打印第一个1
        end loop;
        dbms_output.put_line(''); --打印换行
    end loop;
END;
/
```

- 运行结果

![](https://gitlab.com/zsm922/oracle/-/raw/main/test4/pict1.png)



- 将源代码转为hr用户下的一个存储过程Procedure，名称为YHTriange

![](https://gitlab.com/zsm922/oracle/-/raw/main/test4/pict2.png)

![](https://gitlab.com/zsm922/oracle/-/raw/main/test4/pict3.png)



- 运行这个存储过程

![](https://gitlab.com/zsm922/oracle/-/raw/main/test4/pict4.png)



- 创建YHTriange存储过程以及运行这个存储过程的SQL语句

```sql
-- 创建一个名为 YHTriangle 的存储过程，输入参数为 N
create or replace procedure YHTriangle(N in integer) 
is
   -- 定义一个名为 t_number 的 varray 类型，长度为 100，其中存储整数
   type t_number is varray (100) of integer not null;
   
   -- 声明一些变量
   i integer;  -- 用于循环计数
   j integer;  -- 用于循环计数
   spaces varchar2(30) :='   ';  -- 三个空格，用于打印时分隔数字
   
   -- 初始化数组
   rowArray t_number := t_number();
begin
   -- 先打印前两行
   dbms_output.put_line('1');  -- 第一行只有一个 1
   dbms_output.put(rpad(1,9,' '));  -- 第二行有两个 1，用 rpad 函数格式化输出
   dbms_output.put(rpad(1,9,' '));
   dbms_output.put_line('');  -- 换行
   
   -- 初始化数组数据
   for i in 1 .. N loop
        rowArray.extend;
    end loop;
    
    rowArray(1):=1;  -- 第一行只有一个 1
    rowArray(2):=1;  -- 第二行有两个 1
    
    -- 从第三行开始打印
    for i in 3 .. N loop
        rowArray(i):=1;  -- 每行第一个数都为 1
        
        j:=i-1;  -- 准备第 j 行的数组数据，j 为上一行的行号
        
        -- 计算当前行的数值
        while j>1 loop
            rowArray(j):=rowArray(j)+rowArray(j-1);
            j:=j-1;
        end loop;
        
        -- 打印本行数据
        for j in 1 .. i loop
            dbms_output.put(rpad(rowArray(j),9,' '));  -- 使用 rpad 函数格式化输出
        end loop;
        
        dbms_output.put_line('');  -- 换行
    end loop;
end YHTriangle;

-- 开启输出
set serveroutput on;

-- 测试存储过程
declare
   begin
      YHTriangle(5);  -- 调用 YHTriangle 存储过程，打印前 5 行杨辉三角
   end;
```



## 实验总结

在本次实验中，我学习了Oracle PL/SQL语言和存储过程的基本概念和编写方法。通过转化源代码为存储过程的形式，并将其存储起来，我们可以更加方便地调用并使用该功能。本次实验让我深刻认识到PL/SQL语言强大的功能和应用场景，以及如何利用存储过程提高数据库操作的效率和安全性。同时，我也对杨辉三角有了更深入的了解，了解到它是一种重要的数学模型，在组合数学、代数学等领域都有广泛的应用。通过本次实验，我不仅掌握了PL/SQL语言和存储过程的基础知识，也更好地理解了数学与计算机科学的结合，对我的学习和发展具有重要的指导意义。
