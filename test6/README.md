###### 班级：20级软件工程4班
###### 学号：202010414429
###### 姓名：周思藐


# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计 

- 设计一套基于Oracle数据库的商品销售系统的数据库设计方案。
  - 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
  - 设计权限及用户分配方案。至少两个用户。
  - 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
  - 设计一套数据库的备份方案。



## 实验内容

#### 实验准备

- 创建一个新的用户sale_user，并给他授权

```sql
-- 创建用户
CREATE USER sale_user IDENTIFIED BY 123;
-- 授予用户权限
GRANT CONNECT, RESOURCE TO sale_user;
```

![](https://gitlab.com/zsm922/oracle/-/raw/main/test6/pict1.png)

![](https://gitlab.com/zsm922/oracle/-/raw/main/test6/pict2.png)

- 建立一个名为pdborcl_user的新的连接

![](https://gitlab.com/zsm922/oracle/-/raw/main/test6/pict3.png)

#### 一、表及表空间设计方案

1. 为了实现更好的性能和数据管理，我们可以将不同类型的表分配到不同的表空间中。 例如：

- 用户数据表（用户ID、用户名、密码等信息）和产品表（产品ID、名称、描述、价格等信息）可以放在一个表空间中，名为“USERS_PRODUCTS_TS”；
- 订单表（订单ID、用户ID、收货地址、下单时间等信息）和订单明细表（订单ID、产品ID、购买数量、小计等信息）可以放在另一个表空间中，名为“ORDERS_DETAILS_TS”。

```sql
-- 创建表空间 USERS_PRODUCTS_TS
CREATE TABLESPACE USERS_PRODUCTS_TS
DATAFILE 'users_products_ts.dbf'
SIZE 100M
AUTOEXTEND ON
NEXT 50M;

-- 创建表空间 ORDERS_DETAILS_TS
CREATE TABLESPACE ORDERS_DETAILS_TS
DATAFILE 'orders_details_ts.dbf'
SIZE 100M
AUTOEXTEND ON
NEXT 50M;
```

![](https://gitlab.com/zsm922/oracle/-/raw/main/test6/pict4.png)

- 授予用户`sale_user`对表空间`USERS_PRODUCTS_TS`和`ORDERS_DETAILS_TS`的权限

```sql
GRANT RESOURCE, CONNECT TO sale_user;
GRANT UNLIMITED TABLESPACE TO sale_user;
```

![](https://gitlab.com/zsm922/oracle/-/raw/main/test6/pict5.png)

2. 创建了四个表：users、products、orders和order_details，分别对应用户数据、产品数据、订单数据和订单明细数据。每个表都指定了所属的表空间。

具体的表结构：

- 用户表（users）

  | 列名     | 数据类型      | 描述      |
  | -------- | ------------- | --------- |
  | id       | NUMBER(10)    | ID (主键) |
  | username | VARCHAR2(20)  | 用户名    |
  | password | VARCHAR2(20)  | 密码      |
  | address  | VARCHAR2(100) | 地址      |
  | phone    | VARCHAR2(20)  | 电话      |
  | email    | VARCHAR2(50)  | 邮箱      |

- 产品表（products）
  
  | 列名        | 数据类型      | 描述      |
  | ----------- | ------------- | --------- |
  | id          | NUMBER(10)    | ID (主键) |
  | name        | VARCHAR2(50)  | 产品名称  |
  | description | VARCHAR2(100) | 描述      |
  | price       | NUMBER(8,2)   | 价格      |
  | stock       | NUMBER(10)    | 库存量    |
  
- 订单表（orders）
  
  | 列名             | 数据类型      | 描述                       |
  | ---------------- | ------------- | -------------------------- |
  | id               | NUMBER(10)    | ID (主键)                  |
  | order_id         | NUMBER(10)    | 用户ID (外键：users表的ID) |
  | shipping_address | VARCHAR2(200) | 收货地址                   |
  | order_time       | TIMESTAMP(0)  | 下单时间                   |
  | total_price      | NUMBER(8,2)   | 订单总价                   |
  
- 订单明细表（order_details）
  
  | 列名       | 数据类型    | 描述                          |
  | ---------- | ----------- | ----------------------------- |
  | id         | NUMBER(10)  | ID (主键)                     |
  | order_id   | NUMBER(10)  | 订单ID (外键：orders表的ID)   |
  | product_id | NUMBER(10)  | 产品ID (外键：products表的ID) |
  | quantity   | NUMBER(10)  | 购买数量                      |
  | subtotal   | NUMBER(8,2) | 小计                          |

```sql
-- 用户表（users）
CREATE TABLE users (
  id NUMBER(10) PRIMARY KEY,
  username VARCHAR2(20) NOT NULL,
  password VARCHAR2(20) NOT NULL,
  address VARCHAR2(100) NOT NULL,
  phone VARCHAR2(20) NOT NULL,
  email VARCHAR2(50) NOT NULL
)
TABLESPACE USERS_PRODUCTS_TS;

-- 产品表（products）
CREATE TABLE products (
  id NUMBER(10) PRIMARY KEY,
  name VARCHAR2(50) NOT NULL,
  description VARCHAR2(200) NOT NULL,
  price NUMBER(8,2) NOT NULL,
  stock NUMBER(10) NOT NULL
)
TABLESPACE USERS_PRODUCTS_TS;

-- 订单表（orders）
CREATE TABLE orders (
  id NUMBER(10) PRIMARY KEY,
  user_id NUMBER(10) NOT NULL,
  shipping_address VARCHAR2(200) NOT NULL,
  order_time TIMESTAMP(0) DEFAULT CURRENT_TIMESTAMP NOT NULL,
  total_price NUMBER(8,2) NOT NULL,
  CONSTRAINT fk_orders_user_id FOREIGN KEY (user_id) REFERENCES users(id)
)
TABLESPACE ORDERS_DETAILS_TS;

-- 订单明细表（order_details）
CREATE TABLE order_details (
  id NUMBER(10) PRIMARY KEY,
  order_id NUMBER(10) NOT NULL,
  product_id NUMBER(10) NOT NULL,
  quantity NUMBER(10) NOT NULL, 
  subtotal NUMBER(8,2) NOT NULL,
  CONSTRAINT fk_order_details_order_id FOREIGN KEY (order_id) REFERENCES orders(id),
  CONSTRAINT fk_order_details_product_id FOREIGN KEY (product_id) REFERENCES products(id)
)
TABLESPACE ORDERS_DETAILS_TS;
```

![](https://gitlab.com/zsm922/oracle/-/raw/main/test6/pict6.png)

3. 授予用户操作表格的权限

```sql
GRANT SELECT, INSERT, UPDATE, DELETE ON users TO sale_user;
GRANT SELECT, INSERT, UPDATE, DELETE ON products TO sale_user;
GRANT SELECT, INSERT, UPDATE, DELETE ON orders TO sale_user;
GRANT SELECT, INSERT, UPDATE, DELETE ON order_details TO sale_user;
```

![](https://gitlab.com/zsm922/oracle/-/raw/main/test6/pict7.png)

4. 创建出的表为空表，需要插入数据，分别向users、products、orders和order_details表中插入了3万条数据，共计12w条数据

```sql
-- 插入用户数据
DECLARE
  i NUMBER := 1;
BEGIN
  WHILE i <= 30000 LOOP
    INSERT INTO users(id, username, password, address, phone, email)
    VALUES(i, 'user' || i, 'password' || i, 'address' || i, 'phone' || i, 'email' || i || '@example.com');
    i := i + 1;
  END LOOP;
  COMMIT;
END;
/

-- 插入产品数据
DECLARE
  i NUMBER := 1;
BEGIN
  WHILE i <= 30000 LOOP
    INSERT INTO products(id, name, description, price, stock)
    VALUES(i, 'product' || i, 'description' || i, i * 1.5, i * 10);
    i := i + 1;
  END LOOP;
  COMMIT;
END;
/

-- 插入订单数据
DECLARE
  i NUMBER := 1;
BEGIN
  WHILE i <= 30000 LOOP
    INSERT INTO orders(id, user_id, shipping_address, total_price)
    VALUES(i, i, 'address' || i, i * 10);
    i := i + 1;
  END LOOP;
  COMMIT;
END;
/

-- 插入订单明细数据
DECLARE
  i NUMBER := 1;
BEGIN
  WHILE i <= 30000 LOOP
    INSERT INTO order_details(id, order_id, product_id, quantity, subtotal)
    VALUES(i, i, i, i * 2, i * 3.5);
    i := i + 1;
  END LOOP;
  COMMIT;
END;
/
```

![](https://gitlab.com/zsm922/oracle/-/raw/main/test6/pict8.png)

查看表orders数据，数据成功插入表中

![](https://gitlab.com/zsm922/oracle/-/raw/main/test6/pict9.png)



#### 二、设计权限及用户分配方案

在这个系统中，我们可以创建两个用户：一个是管理员用户，另一个是普通用户。管理员用户具有所有操作权限，包括添加/删除/修改用户、添加/删除/修改产品、查看订单信息等；普通用户只能浏览商品和下单。

为了实现这个权限分配，我们可以创建两个角色（admin_role和user_role），并将相应的权限授予给这些角色。然后，将管理员用户指定为admin_role的成员，将普通用户指定为user_role的成员。

1. 创建角色

```sql
CREATE ROLE admin_role;
CREATE ROLE user_role;
```

![](https://gitlab.com/zsm922/oracle/-/raw/main/test6/pict10.png)

2. 授予权限

```sql
-- 权限授予给admin_role
GRANT CREATE SESSION, CREATE TABLE, CREATE SEQUENCE, CREATE PROCEDURE, CREATE TRIGGER TO admin_role;
GRANT SELECT, INSERT, UPDATE, DELETE ON users TO admin_role;
GRANT SELECT, INSERT, UPDATE, DELETE ON products TO admin_role;
GRANT SELECT, INSERT, UPDATE, DELETE ON orders TO admin_role;
GRANT SELECT, INSERT, UPDATE, DELETE ON order_details TO admin_role;

-- 权限授予给user_role
GRANT CREATE SESSION TO user_role;
GRANT SELECT, INSERT, UPDATE, DELETE ON users TO user_role;
GRANT SELECT, INSERT, UPDATE, DELETE ON products TO user_role;
GRANT SELECT, INSERT, UPDATE, DELETE ON orders TO user_role;
GRANT SELECT, INSERT, UPDATE, DELETE ON order_details TO user_role;
```

![](https://gitlab.com/zsm922/oracle/-/raw/main/test6/pict11.png)

上面的代码将一些常见的权限授予给admin_role和user_role角色。

- 对于admin_role，授权包括创建会话、创建表、创建序列、创建存储过程、创建触发器以及对users、products、orders和order_details表的SELECT、INSERT、UPDATE和DELETE操作；
- 对于user_role，授权包括创建会话以及对users、products、orders和order_details表的SELECT、INSERT、UPDATE和DELETE操作。

3. 创建用户并分配角色

```sql
CREATE USER myadmin IDENTIFIED BY password;
CREATE USER myuser IDENTIFIED BY password;

GRANT admin_role TO myadmin;
GRANT user_role TO myuser;
```

![](https://gitlab.com/zsm922/oracle/-/raw/main/test6/pict12.png)

上面的代码创建了两个用户：admin和user，并设置了密码。然后，将admin_role角色授予admin用户，将user_role角色授予user用户。



#### 三、建立程序包及存储过程和函数

为了实现比较复杂的业务逻辑，我们可以在数据库中建立一个程序包，并用PL/SQL语言设计一些存储过程和函数。这个程序包可以提供以下功能：

- 添加/删除/修改用户信息
- 添加/删除/修改产品信息
- 查看订单信息
- 下单
- 取消订单

```sql
create or replace PACKAGE my_package AS

  -- 添加用户信息
  PROCEDURE add_user(
    p_username IN users.username%TYPE,
    p_password IN users.password%TYPE,
    p_address IN users.address%TYPE,
    p_phone IN users.phone%TYPE,
    p_email IN users.email%TYPE
  );

  -- 删除用户信息
  PROCEDURE delete_user(p_user_id IN users.id%TYPE);

  -- 修改用户信息
  PROCEDURE update_user(
    p_user_id IN users.id%TYPE,
    p_username IN users.username%TYPE DEFAULT NULL,
    p_password IN users.password%TYPE DEFAULT NULL,
    p_address IN users.address%TYPE DEFAULT NULL,
    p_phone IN users.phone%TYPE DEFAULT NULL,
    p_email IN users.email%TYPE DEFAULT NULL
  );

  -- 添加产品信息
  PROCEDURE add_product(
    p_name IN products.name%TYPE,
    p_description IN products.description%TYPE,
    p_price IN products.price%TYPE,
    p_stock IN products.stock%TYPE
  );

  -- 删除产品信息
  PROCEDURE delete_product(p_product_id IN products.id%TYPE);

  -- 修改产品信息
  PROCEDURE update_product(
    p_product_id IN products.id%TYPE,
    p_name IN products.name%TYPE DEFAULT NULL,
    p_description IN products.description%TYPE DEFAULT NULL,
    p_price IN products.price%TYPE DEFAULT NULL,
    p_stock IN products.stock%TYPE DEFAULT NULL
  );

  -- 查看订单信息
  FUNCTION get_order_details(p_order_id IN orders.id%TYPE)
    RETURN SYS_REFCURSOR;

  -- 下单
  PROCEDURE place_order(
    p_user_id IN orders.user_id%TYPE,
    p_shipping_address IN orders.shipping_address%TYPE,
    p_product_id IN order_details.product_id%TYPE,
    p_quantity IN order_details.quantity%TYPE
  );

  -- 取消订单
  PROCEDURE cancel_order(p_order_id IN orders.id%TYPE);

END my_package;
```

![](https://gitlab.com/zsm922/oracle/-/raw/main/test6/pict13.png)

```sql
create or replace PACKAGE BODY my_package AS

  -- 查看订单信息
  FUNCTION get_order_details(p_order_id IN orders.id%TYPE)
    RETURN SYS_REFCURSOR
  IS
    v_cursor SYS_REFCURSOR;
  BEGIN
    OPEN v_cursor FOR
      SELECT od.id,
             p.name,
             od.quantity,
             od.subtotal
        FROM order_details od
        JOIN products p ON od.product_id = p.id
       WHERE od.order_id = p_order_id;
    RETURN v_cursor;
  END;

  -- 下单
  PROCEDURE place_order(
    p_user_id IN orders.user_id%TYPE,
    p_shipping_address IN orders.shipping_address%TYPE,
    p_product_id IN order_details.product_id%TYPE,
    p_quantity IN order_details.quantity%TYPE
  )
  IS
    v_order_id orders.id%TYPE;
    v_subtotal order_details.subtotal%TYPE;
  BEGIN
    -- 添加订单记录
    INSERT INTO orders (id, user_id, shipping_address, total_price)
    VALUES (orders_seq.NEXTVAL, p_user_id, p_shipping_address, 0);

    -- 获取订单ID和商品价格
    SELECT MAX(id) INTO v_order_id FROM orders WHERE user_id = p_user_id;
    SELECT price * p_quantity INTO v_subtotal FROM products WHERE id = p_product_id;

    -- 更新订单总价格
    UPDATE orders SET total_price = total_price + v_subtotal WHERE id = v_order_id;

    -- 添加订单明细记录
    INSERT INTO order_details (id, order_id, product_id, quantity, subtotal)
    VALUES (order_details_seq.NEXTVAL, v_order_id, p_product_id, p_quantity, v_subtotal);

    COMMIT;
  END;

  -- 取消订单
  PROCEDURE cancel_order(p_order_id IN orders.id%TYPE)
  IS
  BEGIN
    -- 删除订单明细记录
    DELETE FROM order_details WHERE order_id = p_order_id;

    -- 删除订单记录
    DELETE FROM orders WHERE id = p_order_id;

    COMMIT;
  END;

  -- 添加用户信息
  PROCEDURE add_user(
    p_username IN users.username%TYPE,
    p_password IN users.password%TYPE,
    p_address IN users.address%TYPE,
    p_phone IN users.phone%TYPE,
    p_email IN users.email%TYPE
  )
  IS
  BEGIN
    INSERT INTO users (id, username, password, address, phone, email)
    VALUES (users_seq.NEXTVAL, p_username, p_password, p_address, p_phone, p_email);
    COMMIT;
  END;

  -- 删除用户信息
  PROCEDURE delete_user(p_user_id IN users.id%TYPE)
  IS
  BEGIN
    DELETE FROM users WHERE id = p_user_id;
    COMMIT;
  END;

  -- 修改用户信息
  PROCEDURE update_user(
    p_user_id IN users.id%TYPE,
    p_username IN users.username%TYPE DEFAULT NULL,
    p_password IN users.password%TYPE DEFAULT NULL,
    p_address IN users.address%TYPE DEFAULT NULL,
    p_phone IN users.phone%TYPE DEFAULT NULL,
    p_email IN users.email%TYPE DEFAULT NULL
  )
  IS
  BEGIN
    UPDATE users SET
      username = COALESCE(p_username, username),
      password = COALESCE(p_password, password),
      address = COALESCE(p_address, address),
      phone = COALESCE(p_phone, phone),
      email = COALESCE(p_email, email)
    WHERE id = p_user_id;
    COMMIT;
  END;

  -- 添加产品信息
  PROCEDURE add_product(
    p_name IN products.name%TYPE,
    p_description IN products.description%TYPE,
    p_price IN products.price%TYPE,
    p_stock IN products.stock%TYPE
  )
  IS
  BEGIN
    INSERT INTO products (id, name, description, price, stock)
    VALUES (products_seq.NEXTVAL, p_name, p_description, p_price, p_stock);
    COMMIT;
  END;

  -- 删除产品信息
  PROCEDURE delete_product(p_product_id IN products.id%TYPE)
  IS
  BEGIN
    DELETE FROM products WHERE id = p_product_id;
    COMMIT;
  END;

  -- 修改产品信息
  PROCEDURE update_product(
    p_product_id IN products.id%TYPE,
    p_name IN products.name%TYPE DEFAULT NULL,
    p_description IN products.description%TYPE DEFAULT NULL,
    p_price IN products.price%TYPE DEFAULT NULL,
    p_stock IN products.stock%TYPE DEFAULT NULL
  )
  IS
  BEGIN
    UPDATE products SET
      name= COALESCE(p_name, name),
      description = COALESCE(p_description, description),
      price = COALESCE(p_price, price),
      stock = COALESCE(p_stock, stock)
    WHERE id = p_product_id;
  COMMIT;
  END;

END my_package;
```

![](https://gitlab.com/zsm922/oracle/-/raw/main/test6/pict14.png)

对程序包进行调用：

- 删除用户信息

```sql
BEGIN
  my_package.delete_user(1);
END;
```

![](https://gitlab.com/zsm922/oracle/-/raw/main/test6/pict15.png)

- 修改用户信息

```sql
-- 修改用户信息
DECLARE
  CURSOR c_user IS 
    SELECT *
    FROM users
    WHERE id = 2;
  v_user c_user%ROWTYPE;
BEGIN
  my_package.update_user(p_user_id => 2, p_phone => '555-1234');
  OPEN c_user;
  FETCH c_user INTO v_user;
  DBMS_OUTPUT.PUT_LINE('User ID: ' || v_user.id);
  DBMS_OUTPUT.PUT_LINE('Name: ' || v_user.username); 
  DBMS_OUTPUT.PUT_LINE('Phone: ' || v_user.phone);
  CLOSE c_user;
END;
```

![](https://gitlab.com/zsm922/oracle/-/raw/main/test6/pict16.png)

- 修改产品信息

```sql
-- 修改产品信息
DECLARE
  product_id NUMBER := 2;
  new_stock_level NUMBER := 50;
  product_name VARCHAR2(100);
  product_price NUMBER;
  product_stock NUMBER;
BEGIN
  my_package.update_product(p_product_id => product_id, p_stock => new_stock_level);

  -- 获取更新后的商品信息
  SELECT name, price, stock INTO product_name, product_price, product_stock FROM products WHERE id = product_id;

  -- 打印更新后的商品信息到控制台
  DBMS_OUTPUT.PUT_LINE('更新后的商品信息：');
  DBMS_OUTPUT.PUT_LINE('- 商品名称：' || product_name);
  DBMS_OUTPUT.PUT_LINE('- 价格：¥' || product_price);
  DBMS_OUTPUT.PUT_LINE('- 库存：' || product_stock);
END;
```

![](https://gitlab.com/zsm922/oracle/-/raw/main/test6/pict17.png)

- 取消订单

```sql
-- 取消订单
DECLARE
  order_id NUMBER := 1; -- 假设要取消的订单ID为1
  order_total NUMBER(8,2);
BEGIN
  dbms_output.put_line('正在取消订单...');
  my_package.cancel_order(order_id);

  -- 获取已取消的订单信息，如果订单不存在则跳过
  BEGIN
    SELECT total_price INTO order_total FROM orders WHERE id = order_id;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      dbms_output.put_line('订单不存在。');
      RETURN;
  END;

  -- 打印已取消的订单信息到控制台
  DBMS_OUTPUT.PUT_LINE('已取消的订单信息：');
  DBMS_OUTPUT.PUT_LINE('- 订单总价：¥' || order_total);
END;
```

![](https://gitlab.com/zsm922/oracle/-/raw/main/test6/pict18.png)

- 查看订单信息

```sql
-- 查看订单信息
DECLARE
  v_order_id NUMBER := 2;
  v_order_total NUMBER(8,2);
BEGIN
  SELECT total_price INTO v_order_total FROM orders WHERE id = v_order_id;

  DBMS_OUTPUT.PUT_LINE('订单总价：¥' || v_order_total);
END;
```

![](https://gitlab.com/zsm922/oracle/-/raw/main/test6/pict19.png)



#### 四、设计一套数据库备份方案

为了确保数据的安全性，我们需要设计一套数据库备份方案，以防止意外数据损坏或丢失。 我们可以采用定期备份的方式进行备份。例如每天晚上备份一次，在备份之前先停止所有与数据库相关的服务和程序，然后使用Oracle提供的备份工具进行备份。同时，还可以将备份文件复制到另一个服务器或云存储服务中，以便在必要时进行恢复。

清理过时备份和归档日志可以避免存储空间被占用并确保备份的可靠性。使用rman进行备份可以使备份过程更高效、更可靠，并根据需要自动删除过时备份和归档日志。这样做可以确保数据库数据的安全性，并提供恢复能力，以保护数据库免受意外损坏或丢失的风险。

- **将数据库修改为存档模式**

  - 连接数据库

    ```sql
    [oracle@oracle1 ~]$ sqlplus / as sysdba
    ```

  - 查看日志是否是存档模式

    ```sql
    SQL> archive log list
    数据库日志模式             非存档模式
    自动存档             禁用
    存档终点            USE_DB_RECOVERY_FILE_DEST
    最早的联机日志序列     21
    当前日志序列           23
    ```
  
  - 停止数据库
  
    ```sql
    SQL> shutdown immediate
    数据库已经关闭。
    已经卸载数据库。
    ORACLE 例程已经关闭。
    ```

  - 将数据库启动之装载状态
  
    ```sql
    SQL> startup mount
    ORACLE 例程已经启动。
    
    Total System Global Area 1593835520 bytes
    Fixed Size		    8793256 bytes
    Variable Size		 1040188248 bytes
    Database Buffers	  536870912 bytes
    Redo Buffers		    7983104 bytes
    数据库装载完毕。
    ```
  
  - 将日志改为归档模式
  
    ```
    SQL> alter database archivelog;
    
    数据库已更改。
    ```
    
  - 打开数据库
  
    ```sql
    SQL> alter database open;
    
    数据库已更改。
    ```
    
  - 再次查看日志是否为存档模式
  
    ```sql
    SQL> archive log list
    数据库日志模式            存档模式
    自动存档             启用
    存档终点            USE_DB_RECOVERY_FILE_DEST
    最早的联机日志序列     21
    下一个存档日志序列   23
    当前日志序列           23
    ```

![](https://gitlab.com/zsm922/oracle/-/raw/main/test6/pict20.png)

![](https://gitlab.com/zsm922/oracle/-/raw/main/test6/pict21.png)

- 设置rman自动备份

  - 清理过时的备份和归档日志

    ```sql
    rman target /  msglog /app/oracle/rman_back/cleanup.log << EOF
    CROSSCHECK BACKUP;
    DELETE NOPROMPT EXPIRED BACKUP;
    crosscheck archivelog all;
    delete noprompt expired archivelog all;
    CROSSCHECK BACKUPSET;
    DELETE NOPROMPT OBSOLETE;
    exit
    EOF
    ```

 ![](https://gitlab.com/zsm922/oracle/-/raw/main/test6/pict22.png)

  - 备份数据库和归档日志

    ```sql
    rman target /  msglog /app/oracle/rman_back/backup.log << EOF
    RUN {
    CONFIGURE RETENTION POLICY TO RECOVERY WINDOW OF 2 DAYS;
    CONFIGURE CONTROLFILE AUTOBACKUP ON;
    CONFIGURE CONTROLFILE AUTOBACKUP FORMAT FOR DEVICE TYPE DISK TO "/app/oracle/rman_back/%F";
    ALLOCATE CHANNEL CH1 DEVICE TYPE DISK FORMAT "/app/oracle/rman_back/%U";
    BACKUP DATABASE SKIP INACCESSIBLE FILESPERSET 10 PLUS ARCHIVELOG FILESPERSET 20 DELETE ALL INPUT;
    RELEASE CHANNEL CH1;
    }
    exit
    EOF
    ```

![](https://gitlab.com/zsm922/oracle/-/raw/main/test6/pict23.png)



## 实验总结

在本次期末考核中，我设计了一套基于Oracle数据库的商品销售系统。这个系统涵盖了表及表空间设计方案、权限及用户分配方案、程序包及存储过程和函数设计以及备份方案设计。

通过这个实验，我深刻认识到了数据库在企业信息化中的重要性。一个好的数据库设计可以提高业务运营效率、减少人工出错、提升客户体验等多种方面的优势。而相反，一个不良的设计则可能导致系统瘫痪、数据丢失、业务不能正常进行等问题。

首先，在表及表空间设计方案中，我将不同类型的数据表分配到不同的表空间中。这样可以更好地管理数据，并提高系统性能。在这个系统中，我创建了四个表：users、products、orders和order_details，分别对应用户数据、产品数据、订单数据和订单明细数据。每个表都指定了所属的表空间，并向表中插入了3万条数据，共计12w条数据。

其次，在权限及用户分配方案中，我创建了两个角色：admin_role和user_role，并将相应的权限授予给这些角色。管理员用户具有所有操作权限，包括添加/删除/修改用户、添加/删除/修改产品、查看订单信息等；普通用户只能浏览商品和下单。

接着，在程序包及存储过程和函数设计中，我建立了一个程序包，并用PL/SQL语言设计了一些存储过程和函数。这个程序包可以提供以下功能：添加/删除/修改用户信息、添加/删除/修改产品信息、查看订单信息、下单和取消订单。

最后，在备份方案设计中，我采用定期备份的方式进行备份，可以每天晚上备份一次。在备份之前先停止所有与数据库相关的服务和程序，然后使用Oracle提供的备份工具进行备份。同时，还可以将备份文件复制到另一个服务器或云存储服务中，以便在必要时进行恢复。清理过时备份和归档日志可以避免存储空间被占用并确保备份的可靠性。使用rman进行备份可以使备份过程更高效、更可靠，并根据需要自动删除过时备份和归档日志。

总的来说，这个商品销售系统的设计方案非常完善。它不仅考虑了数据管理和性能优化，还具有良好的安全性和灵活性，能够满足各种需求。通过本次实验，我对于Oracle数据库的应用和设计有了更深入的理解和掌握，这对我的专业发展和未来的工作也有重要意义。



