###### 班级：20级软件工程4班
###### 学号：202010414429
###### 姓名：周思藐


# 实验1：SQL语句的执行计划分析与优化指导

## 实验目的

  分析SQL执行计划，执行SQL语句的优化指导。理解分析SQL语句的执行计划的重要作用。

## 实验数据库和用户

  数据库是pdborcl，用户是sys和hr

## 实验内容

- 对Oracle12c中的HR人力资源管理系统中的表进行查询与分析。
- 设计自己的查询语句，并作相应的分析，查询语句不能太简单。执行两个比较复杂的返回相同查询结果数据集的SQL语句，通过分析SQL语句各自的执行计划，判断哪个SQL语句是最优的。最后将你认为最优的SQL语句通过sqldeveloper的优化指导工具进行优化指导，看看该工具有没有给出优化建议。

## 代码执行

- 用户hr默认没有统计权限，打开统计信息功能autotrace时要报错，必须要向用户hr授予以下视图的选择权限：

```text
 v_$sesstat, v_$statname 和 v_$session
```

- 权限分配过程如下

```sql
$ sqlplus sys/123@localhost/pdborcl as sysdba
@$ORACLE_HOME/sqlplus/admin/plustrce.sql
create role plustrace;
GRANT SELECT ON v_$sesstat TO plustrace;
GRANT SELECT ON v_$statname TO plustrace;
GRANT SELECT ON v_$mystat TO plustrace;
GRANT plustrace TO dba WITH ADMIN OPTION;
GRANT plustrace TO hr;
GRANT SELECT ON v_$sql TO hr;
GRANT SELECT ON v_$sql_plan TO hr;
GRANT SELECT ON v_$sql_plan_statistics_all TO hr;
GRANT SELECT ON v_$session TO hr;
GRANT SELECT ON v_$parameter TO hr; 
```


![](https://gitlab.com/zsm922/oracle/-/raw/main/test1/pict1.png)


### 1：查询1

```SQL

$sqlplus hr/123@localhost/pdborcl

set autotrace on

SELECT d.department_name,count(e.job_id)as "部门总人数",
avg(e.salary)as "平均工资"
from hr.departments d,hr.employees e
where d.department_id = e.department_id
and d.department_name in ('IT','Sales')
GROUP BY d.department_name;
```



**查询1结果**

![](https://gitlab.com/zsm922/oracle/-/raw/main/test1/pict2.png)



**查询1 sqldeveloper优化结果**

​	无优化



**查询1 语句分析**

​	查询1语句，通过直接比较两个数据库的id是否相同，以及部门名称是否属于IT和Sales，之后以部门名称分组，进行展示数据。





### 2：查询2

```SQL
set autotrace on

SELECT d.department_name,count(e.job_id)as "部门总人数",
avg(e.salary)as "平均工资"
FROM hr.departments d,hr.employees e
WHERE d.department_id = e.department_id
GROUP BY d.department_name
HAVING d.department_name in ('IT','Sales');
```



**查询2结果**

![](https://gitlab.com/zsm922/oracle/-/raw/main/test1/pict3.png)



**查询2 sqldeveloper优化结果**

| 查找结果                                         | 建议                                                       | 原理                                                         |
| ------------------------------------------------ | ---------------------------------------------------------- | ------------------------------------------------------------ |
| 通过创建一个或多个索引可以改进此语句的执行计划。 | 考虑运行可以改进物理方案设计的访问指导或者创建推荐的索引。 | 创建推荐的索引可以显著地改进此语句的执行计划。但是, 使用典型的 SQL 工作量运行 "访问指导" 可能比单个语句更可取。通过这种方法可以获得全面的索引建议案, 包括计算索引维护的开销和附加的空间消耗。 |



**查询2 语句分析**

​	查询一语句，通过直接比较两个数据库的id是否相同，之后以部门名称分组，最后对数据进行判断，部门名称是否属于IT和Sales。





### 3：自定义查询语句

```sql
set autotrace on

SELECT departments.department_name as "部门", count(employees.job_id) as "部门总人数",
avg(employees.salary) as "平均工资"
FROM employees left join departments 
ON employees.department_id = departments.department_id 
WHERE department_name in ('IT','Sales')
GROUP BY departments.department_name
```



**自定义查询语句 查询结果**

![](https://gitlab.com/zsm922/oracle/-/raw/main/test1/pict4.png)



**优化指导**

![](https://gitlab.com/zsm922/oracle/-/raw/main/test1/pict5.png)



**自定义查询分析**

​	通过左连接的方式，将employees.department_id = departments.department_id 的数据连接起来，再从获取的数据中筛选部门名称再 in ('IT','Sales')中的数据。



## 实验总结

​    在本次实验中，我们使用不同的查询语句对相同条件的数据进行了查询，发现不同的查询语句在效率上存在差异。我们运用了 SQL Developer 查询工具对可以进行优化的查询语句进行了优化，并了解了一些查询语句的优化方法。
